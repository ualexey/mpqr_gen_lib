<?php

include('error_handler.php');

/**
 * Generate Masterpass QR string.
 * @param array ($amount, $referense, $name, $alt_name, $mcc) 
 * @return string (qrstring)
 */
class Mpqr {

    public $channel_id = '40703052';                //TEST
    public $merchant = '03052180223115740504';      //TEST
    public $mcc;                                    //lenghts 4
    public $amount;                                 //minimal currency unit (100 represents 1UAH)
    public $name;                                   //lenghts 1-75
    public $alt_name;                               //lenghts 1-75
    public $city = 'Kiev';                          //lenghts  1-15
    public $reference_number;                       //lenghts 1-12
    public $lifetime = 7200;

    public function __construct($params) {

        date_default_timezone_set('Europe/Kiev');

        if (!isset($params['amount'])) {
            die('invalid amount');
        }

        $this->amount = $params['amount'];
        $this->reference_number = (isset($params['referense'])) ? $params['referense'] : time();
        $this->name = (isset($params['name'])) ? $params['name'] : 'TasLinkTest';
        $this->alt_name = (isset($params['alt_name'])) ? $params['alt_name'] : 'ТасЛинкТест';
        $this->mcc = (isset($params['mcc'])) ? $params['mcc'] : '5942';
    }

    public function generate() {

        $payload_format = '000201';
        $initiation_method = '010212';
        $mcc = '5204' . $this->mcc;
        $currency = '5303980';
        $amount = '54' . $this->lenghtCalc($this->amount) . $this->amount;
        $country_code = '5802UA';
        $merchant_name = '59' . $this->lenghtCalc($this->name) . $this->name;
        $merchant_city = '60' . $this->lenghtCalc($this->city) . $this->city;

        $language_preference = '0002UK';
        $alternate_name = '01' . str_pad(mb_strlen($this->alt_name), 2, 0, STR_PAD_LEFT) . $this->alt_name;
        $merchant_information = '64' . mb_strlen($language_preference . $alternate_name) . $language_preference . $alternate_name;

        $unique_identifier = '0017ua.mastercard.www';
        $p2p = '010200';
        $channel_id = '02' . $this->lenghtCalc($this->channel_id) . $this->channel_id;
        $merchant_id = '03' . $this->lenghtCalc($this->merchant) . $this->merchant;
        $rrn = '04' . $this->lenghtCalc($this->reference_number) . $this->reference_number;
        $amount_type = '050201';
        $qr_parameters_data = $unique_identifier . $p2p . $channel_id . $merchant_id . $rrn . $amount_type;
        $qr_parameters = '80' . $this->lenghtCalc($qr_parameters_data) . $qr_parameters_data;

        $gui = '0017ua.mastercard.www';
        $today = date("YmdHis");
        $start_date = '0117' . $today . '000';
        $str_lifetime = '02' . $this->lenghtCalc($this->lifetime) . $this->lifetime;
        $tag81 = $gui . $start_date . $str_lifetime;
        $tag81 = '81' . $this->lenghtCalc($tag81) . $tag81;

        $pre_crc = '6304';

        $data = $payload_format . $initiation_method . $mcc . $currency . $amount . $country_code . $merchant_name . $merchant_city .
                $merchant_information . $qr_parameters . $tag81 . $pre_crc;

        $crc_nopad = dechex($this->crc16($data));

        $crc = str_pad($crc_nopad, 4, 0, STR_PAD_LEFT);

        $qrstring = $data . $crc;

        return $qrstring;
    }

    private function crc16($sStr, $aParams = array()) {
        $aDefaults = array(
            "polynome" => 0x1021,
            "init" => 0xFFFF,
            "xor_out" => 0x0,
        );
        foreach ($aDefaults as $key => $val) {
            if (!isset($aParams[$key])) {
                $aParams[$key] = $val;
            }
        }
        $sStr .= "";
        $crc = $aParams['init'];
        $len = strlen($sStr);
        $i = 0;

        while ($len--) {
            $crc ^= ord($sStr[$i++]) << 8;
            $crc &= 0xFFFF;

            for ($j = 0; $j < 8; $j++) {
                $crc = ($crc & 0x8000) ? ($crc << 1) ^ $aParams['polynome'] : $crc << 1;
                $crc &= 0xFFFF;
            }
        }
        $crc ^= $aParams['xor_out'];

        return $crc;
    }

    private function lenghtCalc($data) {
        return str_pad(strlen($data), 2, 0, STR_PAD_LEFT);
    }

}

$init = new Mpqr([
    'amount' => 100,
    'referense' => time(),
        ]);

echo($init->generate());
