<?php

error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 'On');
ob_start();
set_error_handler("error_handler");
register_shutdown_function('fatal_error_handler');

function error_handler($errno, $errstr, $errfile, $errline, $fatal = false) {
    if (error_reporting() & $errno) {
        $errors = array(
            E_ERROR => 'E_ERROR',
            E_WARNING => 'E_WARNING',
            E_PARSE => 'E_PARSE',
            E_NOTICE => 'E_NOTICE',
            E_CORE_ERROR => 'E_CORE_ERROR',
            E_CORE_WARNING => 'E_CORE_WARNING',
            E_COMPILE_ERROR => 'E_COMPILE_ERROR',
            E_COMPILE_WARNING => 'E_COMPILE_WARNING',
            E_USER_ERROR => 'E_USER_ERROR',
            E_USER_WARNING => 'E_USER_WARNING',
            E_USER_NOTICE => 'E_USER_NOTICE',
            E_STRICT => 'E_STRICT',
            E_RECOVERABLE_ERROR => 'E_RECOVERABLE_ERROR',
            E_DEPRECATED => 'E_DEPRECATED',
            E_USER_DEPRECATED => 'E_USER_DEPRECATED',
        );

        if ($fatal) {
            $LogNotice = ("<b>{$errors[$errno]}</b>[$errno] $errstr ($errfile на $errline строке)<br />\n");
            print_r($LogNotice);
            //Request::Fatal();
        } else {
            $LogNotice = ("<b>{$errors[$errno]}</b>[$errno] $errstr ($errfile на $errline строке)<br />\n");
            print_r($LogNotice);
        }
    }
    return false;
}

function fatal_error_handler() {
    if ($error = error_get_last() AND $error['type'] & ( E_ERROR | E_PARSE | E_COMPILE_ERROR | E_CORE_ERROR)) {
        ob_end_clean();
        error_handler($error['type'], $error['message'], $error['file'], $error['line'], 1);
    } else {
        ob_end_flush();
    }
    
}